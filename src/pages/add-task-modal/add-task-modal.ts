import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams, Events } from 'ionic-angular';

/**
* Generated class for the AddTaskModalPage page.
*
* See https://ionicframework.com/docs/components/#navigation for more info on
* Ionic pages and navigation.
*/

@IonicPage()
@Component({
  selector: 'page-add-task-modal',
  templateUrl: 'add-task-modal.html',
})
export class AddTaskModalPage {

  task = "Please enter a title";

  constructor(public navCtrl: NavController, public navParams: NavParams, private events: Events) {
  }

  cancel() {
    this.navCtrl.goToRoot();
  }

  submit() {
    if (this.task.length === 0) return this.events.publish('toast', "You have to fill in a title!", 2000);
    if (this.navCtrl.length() > 1) {
      this.navCtrl.pop().then(() => {
        this.events.publish('submit',  this.task);
      })
    }
    else {
      this.navCtrl.goToRoot();
    }
  }

}
