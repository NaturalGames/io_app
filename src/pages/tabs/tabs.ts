import { Component } from '@angular/core';

import { DonePage } from '../done/done';
import { SettingsPage } from '../settings/settings';
import { OpenPage } from '../open/open';
import { HelpPage } from '../help/help';

@Component({
  templateUrl: 'tabs.html'
})
export class TabsPage {

  tab1Root = OpenPage;
  tab2Root = DonePage;
  tab3Root = SettingsPage;
  tab4Root = HelpPage;

  constructor() {

  }
}
