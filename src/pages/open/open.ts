import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams, Events } from 'ionic-angular';
import { SaveTasksProvider } from '../../providers/save-tasks/save-tasks';

@IonicPage()
@Component({
  selector: 'page-open',
  templateUrl: 'open.html',
})
export class OpenPage {


  constructor(public navCtrl: NavController, public navParams: NavParams, private stp: SaveTasksProvider, private events: Events) {
  }

  openModal() {
    let stp_ref = this.stp;
    this.events.subscribe('submit', (paramsVar) => {
        stp_ref.addTask(paramsVar);
        this.events.unsubscribe('submit');
    })
    this.navCtrl.push('AddTaskModalPage');
  }

  delete(task) {
    this.stp.deleteTask(task);
  }

  done(task) {
    this.stp.doneTask(task);
  }

}
