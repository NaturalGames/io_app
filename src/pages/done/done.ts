import { Component } from '@angular/core';
import { NavController } from 'ionic-angular';
import { SaveTasksProvider } from '../../providers/save-tasks/save-tasks';

@Component({
  selector: 'page-done',
  templateUrl: 'done.html'
})
export class DonePage {

  constructor(public navCtrl: NavController, private stp: SaveTasksProvider,) {

  }

  delete(task) {
    this.stp.deleteTask(task);
  }

}
