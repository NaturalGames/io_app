import { Injectable } from '@angular/core';
import { Events } from 'ionic-angular';

@Injectable()
export class SaveTasksProvider {

  tasks = [];
  task = {
    id: "",
    task: "",
    done: false
  };

  constructor(private events: Events) {
    this.tasks = JSON.parse(localStorage.getItem('tasks')) || [];

    events.subscribe('deleteAll', () => {
      this.deleteAll();
      events.publish('toast', "Successfully deleted all tasks!", 2000);
    });
  }

  uuid(){
    return 'xxxxxxxx-xxxx-4xxx-yxxx-xxxxxxxxxxxx'.replace(/[xy]/g, function (c) {
      var r = Math.random() * 16 | 0, v = c === 'x' ? r : (r & 0x3 | 0x8);
      return v.toString(16);
    })
  }

  addTask(task) {
    this.tasks.push({id: this.uuid(), task: task, done: false});
    localStorage.setItem('tasks', JSON.stringify(this.tasks));
    this.events.publish('toast', "Successfully added!", 2000);
  }

  doneTask(task){
    task.done = true;
    localStorage.setItem('tasks', JSON.stringify(this.tasks));
    this.events.publish('toast', "Marked as complete", 2000);
  }

  deleteTask(i) {
    this.tasks.splice(i, 1);
    localStorage.setItem('tasks', JSON.stringify(this.tasks));
    this.events.publish('toast', "Deleted task!", 2000);
  }

  deleteAll() {
    this.tasks = [];
    localStorage.removeItem('tasks');
  }

}
